Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2009, 2011, 2016, Guillem Jover <guillem@hadrons.org>
License: BSD-2-Clause-NetBSD or BSD-2-clause or BSD-3-clause or Beerware or ISC or public-domain

Files: m4/*
Copyright:
 Copyright © 2009, 2011, 2016 Guillem Jover <guillem@hadrons.org>
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
 THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: aclocal.m4
Copyright: 1996-2021, Free Software Foundation, Inc.
License: FSFULLR

Files: build-aux/*
Copyright: 1996-2021, Free Software Foundation, Inc.
License: GPL-2+ with Autoconf-data exception

Files: build-aux/install-sh
Copyright: 1994, X Consortium
License: X11

Files: build-aux/ltmain.sh
Copyright: 1996-2019, 2021, 2022, Free Software Foundation, Inc.
License: (Expat or GPL-2+) with Libtool exception

Files: configure
Copyright: 1992-1996, 1998-2017, 2020, 2021, Free Software Foundation
License: FSFUL

Files: debian/*
Copyright: 2009, 2011, 2015, 2016, 2018, 2019, 2021, 2023, Guillem Jover <guillem@hadrons.org>
License: BSD-3-clause

Files: get-version
Copyright: 2009, 2011, 2015, 2016, 2018, 2019, 2021, 2023, Guillem Jover <guillem@hadrons.org>
License: BSD-3-clause

Files: include/*
Copyright: 2009, 2011, 2015, 2016, 2018, 2019, 2021, 2023, Guillem Jover <guillem@hadrons.org>
License: BSD-3-clause

Files: include/md4.h
 include/md5.h
Copyright: no-info-found
License: public-domain

Files: include/rmd160.h
Copyright: 2001, Markus Friedl.
License: BSD-2-clause

Files: include/sha2.h
Copyright: 2000, 2001, Aaron D. Gifford
License: BSD-3-clause

Files: m4/libtool.m4
Copyright: 1996-2001, 2003-2019, 2021, 2022, Free Software
License: (FSFULLR or GPL-2) with Libtool exception

Files: m4/ltoptions.m4
 m4/lt~obsolete.m4
Copyright: 2004, 2005, 2007-2009, 2011-2019, 2021, 2022, Free
License: FSFULLR

Files: m4/ltsugar.m4
Copyright: 2004, 2005, 2007, 2008, 2011-2019, 2021, 2022, Free Software
License: FSFULLR

Files: m4/ltversion.m4
Copyright: 2004, 2011-2019, 2021, 2022, Free Software Foundation
License: FSFULLR

Files: man/*
Copyright: no-info-found
License: Beerware

Files: man/libmd.7
Copyright: 2009, 2011, 2015, 2016, 2018, 2019, 2021, 2023, Guillem Jover <guillem@hadrons.org>
License: BSD-3-clause

Files: man/rmd160.3
 man/sha1.3
 man/sha2.3
Copyright: 1997, 2003, 2004, Todd C. Miller <Todd.Miller@courtesan.com>
License: ISC

Files: src/*
Copyright: no-info-found
License: Beerware

Files: src/local-link.h
Copyright: 2009, 2011, 2015, 2016, 2018, 2019, 2021, 2023, Guillem Jover <guillem@hadrons.org>
License: BSD-3-clause

Files: src/md2.c
Copyright: 2001, The NetBSD Foundation, Inc.
License: BSD-2-Clause-NetBSD or BSD-2-clause

Files: src/md4.c
 src/md5.c
Copyright: no-info-found
License: public-domain

Files: src/rmd160.c
Copyright: 2001, Markus Friedl.
License: BSD-2-clause

Files: src/sha2.c
Copyright: 2000, 2001, Aaron D. Gifford
License: BSD-3-clause

Files: test/*
Copyright: 2009, 2011, 2015, 2016, 2018, 2019, 2021, 2023, Guillem Jover <guillem@hadrons.org>
License: BSD-3-clause

Files: Makefile.in include/Makefile.in man/Makefile.in src/Makefile.in test/Makefile.in
Copyright:
 Copyright © 2009, 2011, 2016 Guillem Jover <guillem@hadrons.org>
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
 THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
